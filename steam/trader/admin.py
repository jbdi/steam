# coding: utf8
from django.contrib import admin

from steam.trader.models import CsgoTMItem

admin.site.register(CsgoTMItem, admin.ModelAdmin)
