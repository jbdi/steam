# coding: utf-8
# from __future__ import unicode_literals
import csv
from datetime import datetime
import sys
import time
import os

from django.core.management.base import BaseCommand
from django.conf import settings
from django.core.serializers.json import json

import requests
import pytz

from steam.trader.models import (CsgoTMItem, CsgoTMRarity,
    CsgoTMQuality, CsgoTMSlot)
# ['c_classid','c_instanceid','c_price','c_offers','c_popularity','c_rarity','c_quality','c_heroid','c_slot','c_stickers','c_market_name','c_name_color','c_price_updated','c_pop']
def download_file(url):
    local_filename = url.split('/')[-1]
    local_file = settings.BASE_DIR + '/' + local_filename

    if os.path.exists(local_file):
        return None

    # NOTE the stream=True parameter
    r = requests.get(url, stream=True)
    with open(local_file, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)
                #f.flush() commented by recommendation from J.F.Sebastian
    return local_file


IMPORTED_ITEMS_ID = []

class Command(BaseCommand):
    # сделать отдельной процедурой подсчет(перерасчет) кол-ва товаров в категории
    def handle(self, *args, **options):
        start_time = time.time()

        items_remote_filename = requests.get('https://csgo.tm/itemdb/current_730.json').json()['db']
        csv_file_url = 'https://csgo.tm/itemdb/' + items_remote_filename

        local_file = download_file(csv_file_url)
        print local_file
        if local_file:
            csv_reader(local_file, run_import, **options)
        else:
            print 'File already exists'

        print("--- %s seconds ---" % (time.time() - start_time))

        # CsgoTMItem.objects.exclude(id__in=IMPORTED_ITEMS_ID).update(enabled=False)



def run_import(row, **options):

    ALLOWED_FIELDS = (
        'classid',
        'instanceid',
        'price',
        'offers',
        'name_color',
        'popularity',
        'rarity',
        'quality',
        'stickers',
        'heroid',
        'slot',
        'market_name',
        'price_updated',
        'pop',
    )

    row_columns = row.keys()
    for field_name in row_columns:
        new_field_name = field_name.replace('c_', '')
        row[new_field_name] = row.pop(field_name)
        if not new_field_name in ALLOWED_FIELDS:
            del row[new_field_name]

    rarity, _ = CsgoTMRarity.objects.get_or_create(name=row['rarity'].strip().lower())
    quality, _ = CsgoTMQuality.objects.get_or_create(name=row['quality'].strip().lower())
    slot, _ = CsgoTMSlot.objects.get_or_create(name=row['slot'].strip().lower())

    row['rarity'] = rarity
    row['quality'] = quality
    row['slot'] = slot
    row['price'] = float(row['price'].replace(',', '.') or 0)
    row['price_updated'] = datetime.utcfromtimestamp(int(row['price_updated'])).replace(tzinfo=pytz.utc)

    row = {col: val for col, val in row.items()}

    try:
        item, created = CsgoTMItem.objects.update_or_create(classid=row['classid'], defaults=row)
        IMPORTED_ITEMS_ID.append(item.id)
    except Exception, e:
        print 'error with classid %s / %s' % (row.get('classid'), e.message)
        created = False

    counters = {}
    counters['handled'] = 1
    if created:
        counters['created'] = 1
        counters['updated'] = 0
    else:
        counters['created'] = 0
        counters['updated'] = 1
    return counters


def csv_reader(file_path, row_handler, **options):
    verbosity = options.get('verbosity', 0)

    with open(file_path, 'r') as line:
        reader = csv.DictReader(line, delimiter=';')

        # # также можно припихнуть проверку на слишком маленкое кол-во товаров
        # cursor.execute('truncate catalog_item')
        handled = 0
        created = 0
        updated = 0
        for row in reader:
            counters = row_handler(row, **options)
            handled += counters['handled']
            created += counters['created']
            updated += counters['updated']
            if verbosity == 2:
                sys.stdout.write("\rhandled: %d, created: %d, updated: %d" % (handled, created, updated))
                sys.stdout.flush()
        if verbosity == 1:
            sys.stdout.write("\rhandled: %d, created: %d, updated: %d" % (handled, created, updated))
    # sys.stdout("\n\n")
