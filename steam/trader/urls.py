# coding: utf8
from django.conf.urls import url, patterns

# from steam.trader import views


urlpatterns = patterns('steam.trader.views',
    url(r'^/?$', 'my_trades'),
)
