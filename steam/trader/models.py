# coding: utf8
from django.db import models

# https://csgo.tm/itemdb/current_730.json
# https://csgo.tm/botinfo/


class CsgoTMRarity(models.Model):
    name = models.CharField(max_length=100, null=False)

    def __unicode__(self):
        return self.name


class CsgoTMQuality(models.Model):
    name = models.CharField(max_length=100, null=False)

    def __unicode__(self):
        return self.name


class CsgoTMSlot(models.Model):
    name = models.CharField(max_length=100, null=False)

    def __unicode__(self):
        return self.name


class CsgoTMItem(models.Model):
    classid = models.PositiveIntegerField(null=False)
    instanceid = models.PositiveIntegerField(null=False)
    price = models.FloatField(null=False)
    offers = models.PositiveIntegerField(null=False)
    name_color = models.CharField(max_length=255, null=True)
    popularity = models.PositiveIntegerField(null=True)
    rarity = models.ForeignKey('CsgoTMRarity', related_name='csgotm_items', null=True)
    quality = models.ForeignKey('CsgoTMQuality', related_name='csgotm_items', null=True)
    stickers = models.CharField(max_length=255, null=True, default=0)
    heroid = models.PositiveIntegerField(null=True, default=0)
    # категория [Обыч. | StatTrak™ | ★ | ★ StatTrak™ | Сувенир]
    slot = models.ForeignKey('CsgoTMSlot', related_name='csgotm_items', null=True)
    market_name = models.CharField(max_length=255, null=True)
    price_updated = models.DateTimeField()
    pop = models.PositiveIntegerField(null=True)

    def __unicode__(self):
        return '%s, %s, %s' % (self.market_name, self.rarity, self.slot)

